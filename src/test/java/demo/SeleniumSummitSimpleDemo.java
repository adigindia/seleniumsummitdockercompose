package demo;

import org.testng.annotations.Test;

import utils.HelperFunctions;
import utils.MoreHelperFunctions;

import org.testng.annotations.BeforeTest;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;

public class SeleniumSummitSimpleDemo {

	WebDriver driver;

	@BeforeTest
	public void beforeTest() {
		
		driver = HelperFunctions.createAppropriateDriver("chrome");
	}

	@Test
	public void f() {
		
		WebDriverWait wait = new WebDriverWait(driver, 10);
		
		driver.get("https://seleniumsummit21.agiletestingalliance.org/");
		
		wait.until(
				ExpectedConditions.titleContains("#Seleniumsummit21"));
		
		System.out.println(driver.getTitle());

		MoreHelperFunctions.wait(2);
		
		driver.get("https://seleniumsummit21.agiletestingalliance.org/speakers");

		By allSpeakerLocator = By.xpath("//*[@class='elementor-text-editor elementor-clearfix']/h3");
		
		List<WebElement> allspeakers = driver.findElements(allSpeakerLocator);
		
		for (int i = 0; i < allspeakers.size(); i ++) {
			
			WebElement speakerHeader = allspeakers.get(i);
			System.out.println(speakerHeader.getAttribute("innerHTML"));
			MoreHelperFunctions.wait(1);
		}
		
	}

	@AfterTest
	public void afterTest() {
	}

}
