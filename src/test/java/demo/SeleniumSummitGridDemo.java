package demo;

import org.testng.annotations.Test;

import utils.BrowserCreation;
import utils.MoreHelperFunctions;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import java.net.MalformedURLException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;

public class SeleniumSummitGridDemo {

	WebDriver driver;

	@Parameters({"browser","host","port"})
	@BeforeTest
	public void beforeTest(@Optional("chrome") String browser,
			@Optional("http://localhost") String host, 
			@Optional("4444")String port) throws MalformedURLException, InterruptedException {
		
		System.out.println("browser =" + browser);

		System.out.println("host =" + host);

		System.out.println("port =" + port);
		
		driver = BrowserCreation.createRemoteDriver(browser, host , port);	
	}


	@Test
	public void f() {

		WebDriverWait wait = new WebDriverWait(driver, 10);
		
		driver.get("https://seleniumsummit21.agiletestingalliance.org/");
		
		wait.until(
				ExpectedConditions.titleContains("#Seleniumsummit21"));
		
		System.out.println(driver.getTitle());

		MoreHelperFunctions.wait(2);
		
		driver.get("https://seleniumsummit21.agiletestingalliance.org/speakers");

		By allSpeakerLocator = By.xpath("//*[@class='elementor-text-editor elementor-clearfix']/h3");
		
		List<WebElement> allspeakers = driver.findElements(allSpeakerLocator);
		
		for (int i = 0; i < allspeakers.size(); i ++) {
			
			WebElement speakerHeader = allspeakers.get(i);
			System.out.println(speakerHeader.getAttribute("innerHTML"));
			MoreHelperFunctions.wait(1);
		}
		
	}



	@AfterTest
	public void afterTest() {

		driver.quit();
	}

}
